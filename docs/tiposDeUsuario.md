# Principais funcionalidades

## Tipos de usuário
* ### Professor:
    - Pode consultar e controlar as notas dos alunos;
    - Visualiza a lista de alunos matriculados;
    - Exporta relatórios com as notas dos alunos;
    - Visualiza e pode alterar seus dados pessoais.

* ### Aluno:
    - Pode consultar e alterar suas informações pessoais e notas;
    - Imprimir relatório com suas notas;

## Gerenciar Alunos
Métodos responsável por alterar a nota dos alunos (Nota da Disciplina, Nota de Engajamento, Nota de Comportamento e Descrição do Aluno). Onde somente o professor, pode fazer essa alteração.

* Onde lista primeiramente todos os alunos cadastrados;
![lista](images/gerenciar.png)
* . O professor escolhe qual aluno deseja alterar a nota, com base em seu código. 
![escolher aluno](images/gerenciar2.png)
* Irá aparecer todas as informações do aluno.
![informacoes do aluno](images/gerenciar3.png)

* O professor pode escolher entre as opções disponíveis. 
logo após, o professor digita a nota nova.
Em seguida aparece a opção “Continuar Modificação de Notas” ou Retorna ao Menu Anterior.


## Listar todos os alunos
No método listar todos os alunos, ele fica responsável por listar todos os alunos cadastrados no sistema.

* Onde lista primeiramente todos os alunos cadastrados;

* O professor escolhe qual aluno deseja alterar a nota, com base em seu código. 

