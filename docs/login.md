
**Importante:** No primeiro acesso é necessário realizar 
o [Cadastro](cadastro.md) antes de efetuar o login;

1. Escolha a opção de login no menu principal;
2. Insira o seu login de acordo com o solicitado;
3. Insira sua senha;
4. Caso suas credenciais estejam corretas, será exibido o menu de acordo com o seu tipo;
