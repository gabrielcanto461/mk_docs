* Para o professor:
    * É possível imprimir relatórios de qualquer aluno, somente
      inserindo o código do aluno desejado de acordo com a lista exibida;
* Para o aluno:
    * É possível imprimir somente o seu relatório contendo suas notas;

$$
\operatorname{} media=\frac{(nota1 + nota2 + nota3)}{3}
$$