
* O usuário pode se cadastrar como professor ou como aluno;
* Necessário escolher a opção cadastro no menu principal;
* Inserir as informações corretas de acordo informado no menu;

## Dados solicitados

### Professor

| Campos Solicitados | Dados aceitos                                      |
|--------------------|----------------------------------------------------|
| Nome               | Texto sem números, vírgula e pontos                |
| Idade              | Apenas números positivos                           |
| Senha              | Texto com no mínimo 6 caracteres                   |
| Segredos           | Texto com no mínimo 6 caracteres, sem vírgulas     |
| Disciplina         | Texto sem números, vírgula e pontos                |
| Salário            | Número decimal com separador do milhar sendo o `.` |

### Aluno


| Campos Solicitados | Dados aceitos                                  |
|--------------------|------------------------------------------------|
| Nome               | Texto sem números, vírgula e pontos            |
| Login              | Texto com quaisquer caracteres exceto `,`      |
| Idade              | Apenas números positivos                       |
| Senha              | Texto com no mínimo 6 caracteres               |
| Segredos           | Texto com no mínimo 6 caracteres, sem vírgulas |
