# Desafio Técnico - Estágio 2022


# Introdução


## Autores

<img class = "img" src="https://gitlab.com/gabrielcanto461/mk_docs/-/raw/main/docs/images/gabriel.png" alt="Gabriel Silva" width="50"
height="50" />
<p class = "name">Gabriel Augusto</p>
<img class = "img" src="https://gitlab.com/gabrielcanto461/mk_docs/-/raw/main/docs/images/murillo.png" alt="Murilo Schali" width="50"
     height="50" />
<p class = "name">Murilo Schali</p>
<img class = "img" src="https://gitlab.com/gabrielcanto461/mk_docs/-/raw/main/docs/images/yury.jpg" alt="Yury Silva" width="50"
     height="50" />
<p class = "name">Yury Silva</p>

## Propósito

O desafio técnico foi proposto para aplicarmos os conceitos aprendidos no decorrer do estágio até aqui.
Aplicando a maioria dos conceitos aprendido em Java, versionamento de código e alguns outros.

## Desafios

Como foi nosso primeiro trabalho em equipe tivemos alguns problemas com o Git e GitLab,
a "arquitetura" do projeto também foi complicada, pois apesar de ser simples utilizamos 
alguns conceitos de SOLID e MVC.

<p align="center">

 <img src="https://img.shields.io/badge/-Java-blue"/>

 <img src ="https://img.shields.io/badge/Desafio%20T%C3%A9cnico-Conclu%C3%ADdo-orange"/>

</p>